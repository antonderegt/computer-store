# Computer Store

## Demo
Live demo of the [Computer Store](https://antonderegt.gitlab.io/computer-store/).

## Design
See the design in action on [Figma](https://www.figma.com/file/OUToATVd5HwuDQLFqkkf0i/KomputerStore?node-id=1%3A78).
Or take a look at the [pdf export](assets/design/Computer%20Store%20Design.pdf).

## Pre-requisites
Install dependencies:
```
npm install
```

## Development
This command watches for file changes.
```
npm run develop
```

## Build
```
npm run build
```