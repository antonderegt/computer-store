var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var featuresElement = document.getElementById("features");
var laptopsElement = document.getElementById("laptops");
var workButtonElement = document.getElementById("work-button");
var payBalanceElement = document.getElementById("pay-balance");
var bankButtonElement = document.getElementById("bank-button");
var bankBalanceElement = document.getElementById("bank-balance");
var payLoanElement = document.getElementById("pay-loan-button");
// Loan elements
var payLoanButtonElement = document.getElementById("pay-loan-button");
var getLoanButtonElement = document.getElementById("get-loan-button");
var loanLabelElement = document.getElementById("loan-label");
var loanBalanceElement = document.getElementById("loan-balance");
// Details elements
var detailImageElement = document.getElementById("detail-image");
var detailTitleElement = document.getElementById("detail-title");
var detailDescriptionElement = document.getElementById("detail-description");
var detailPriceElement = document.getElementById("detail-price");
var detailBuyElement = document.getElementById("detail-buy-button");
var laptops = [];
var payBalance = 0;
var bankBalance = 0;
var loanBalance = 0;
var salary = 100;
var apiEndpoint = "https://noroff-komputer-store-api.herokuapp.com/computers";
function getLaptops(api) {
    return __awaiter(this, void 0, void 0, function () {
        var response;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, fetch(api)];
                case 1:
                    response = _a.sent();
                    return [4 /*yield*/, response.json()];
                case 2: return [2 /*return*/, _a.sent()];
            }
        });
    });
}
getLaptops(apiEndpoint)
    .then(function (data) {
    laptops = data;
    addLaptopsToMenu(laptops);
})["catch"](function (e) { return console.log("Failed to fetch laptops: " + e); });
var addLaptopsToMenu = function (laptops) {
    laptops.forEach(function (laptop) { return addLaptopToMenu(laptop); });
    featuresElement.innerHTML = "";
    laptops[0].specs.forEach(function (feature) {
        var featureElement = document.createElement("li");
        featureElement.value = feature;
        featureElement.appendChild(document.createTextNode(feature));
        featuresElement.appendChild(featureElement);
    });
    detailImageElement.setAttribute('src', laptops[0].image);
    detailImageElement.setAttribute('alt', laptops[0].title);
    detailTitleElement.innerText = laptops[0].title;
    detailDescriptionElement.innerText = laptops[0].description;
    detailPriceElement.innerText = new Intl.NumberFormat('nl-NL', { style: 'currency', currency: 'EUR' }).format(laptops[0].price);
};
var addLaptopToMenu = function (laptop) {
    var laptopElement = document.createElement("option");
    laptopElement.value = laptop.id;
    laptopElement.appendChild(document.createTextNode(laptop.title));
    laptopsElement.appendChild(laptopElement);
};
var handleLaptopMenuChange = function (e) {
    var selectedLaptop = laptops[e.target.selectedIndex];
    featuresElement.innerHTML = "";
    selectedLaptop.specs.forEach(function (feature) {
        var featureElement = document.createElement("li");
        featureElement.value = feature;
        featureElement.appendChild(document.createTextNode(feature));
        featuresElement.appendChild(featureElement);
    });
    detailImageElement.setAttribute('src', selectedLaptop.image);
    detailImageElement.setAttribute('alt', selectedLaptop.title);
    detailTitleElement.innerText = selectedLaptop.title;
    detailDescriptionElement.innerText = selectedLaptop.description;
    detailPriceElement.innerText = new Intl.NumberFormat('nl-NL', { style: 'currency', currency: 'EUR' }).format(selectedLaptop.price);
};
var handleWork = function () {
    payBalance += salary;
    payBalanceElement.innerText = new Intl.NumberFormat('nl-NL', { style: 'currency', currency: 'EUR' }).format(payBalance);
};
var handleBank = function () {
    if (loanBalance > payBalance / 10) {
        var loanPayment = payBalance / 10;
        loanBalance -= loanPayment;
        payBalance -= loanPayment;
    }
    else if (loanBalance > 0) {
        payBalance -= loanBalance;
        loanBalance = 0;
        showLoanElements(false);
    }
    bankBalance += payBalance;
    payBalance = 0;
    updateBalances();
};
var handleGetLoan = function () {
    if (loanBalance > 0) {
        alert("You already have a loan, please pay it back first.");
        return;
    }
    var amount = parseInt(window.prompt("How much do you need?", ""));
    if (amount === 0 || isNaN(amount)) {
        alert("You didn't receive a loan. If you still want to receive a loan, please try again.");
        return;
    }
    if (amount > bankBalance * 2) {
        alert("Your loan limit is " + 2 * bankBalance + ".");
        return;
    }
    loanBalance = amount;
    bankBalance += amount;
    updateBalances();
    showLoanElements(true);
};
var showLoanElements = function (show) {
    payLoanButtonElement.style.visibility = show ? "visible" : "hidden";
    loanLabelElement.style.visibility = show ? "visible" : "hidden";
    loanBalanceElement.style.visibility = show ? "visible" : "hidden";
    getLoanButtonElement.disabled = show ? true : false;
};
// Make more modulair by supplying the values
var updateBalances = function () {
    payBalanceElement.innerText = new Intl.NumberFormat('nl-NL', { style: 'currency', currency: 'EUR' }).format(payBalance);
    bankBalanceElement.innerText = new Intl.NumberFormat('nl-NL', { style: 'currency', currency: 'EUR' }).format(bankBalance);
    loanBalanceElement.innerText = new Intl.NumberFormat('nl-NL', { style: 'currency', currency: 'EUR' }).format(loanBalance);
};
var handlePayLoan = function () {
    if (loanBalance === 0 || payBalance === 0)
        return;
    if (loanBalance < payBalance) {
        payBalance -= loanBalance;
        loanBalance = 0;
        showLoanElements(false);
    }
    else {
        loanBalance -= payBalance;
        payBalance = 0;
    }
    updateBalances();
};
var handleBuy = function () {
    var selectedLaptop = laptops[laptopsElement.selectedIndex];
    var laptopPrice = selectedLaptop.price;
    if (bankBalance < laptopPrice) {
        alert("Balance too low... Go to work!");
        return;
    }
    bankBalance -= laptopPrice;
    updateBalances();
    alert("Nice you bought a laptop");
};
// All event listeners
laptopsElement.addEventListener("change", handleLaptopMenuChange);
workButtonElement.addEventListener("click", handleWork);
bankButtonElement.addEventListener("click", handleBank);
getLoanButtonElement.addEventListener("click", handleGetLoan);
payLoanButtonElement.addEventListener("click", handlePayLoan);
detailBuyElement.addEventListener("click", handleBuy);
