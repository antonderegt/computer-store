const featuresElement = document.getElementById("features");
const laptopsElement = document.getElementById("laptops") as HTMLSelectElement;
const workButtonElement = document.getElementById("work-button");
const payBalanceElement = document.getElementById("pay-balance");
const bankButtonElement = document.getElementById("bank-button");
const bankBalanceElement = document.getElementById("bank-balance");
const payLoanElement = document.getElementById("pay-loan-button");

// Loan elements
const payLoanButtonElement = document.getElementById("pay-loan-button");
const getLoanButtonElement = document.getElementById("get-loan-button") as HTMLInputElement;
const loanLabelElement = document.getElementById("loan-label");
const loanBalanceElement = document.getElementById("loan-balance");

// Details elements
const detailImageElement = document.getElementById("detail-image");
const detailTitleElement = document.getElementById("detail-title");
const detailDescriptionElement = document.getElementById("detail-description");
const detailPriceElement = document.getElementById("detail-price");
const detailBuyElement = document.getElementById("detail-buy-button");

let laptops = [];
let payBalance = 0;
let bankBalance = 0;
let loanBalance = 0;
const salary = 100;
const apiEndpoint = "https://noroff-komputer-store-api.herokuapp.com/computers";

async function getLaptops(api:string) {
    const response = await fetch(api);
    return await response.json();
}

getLaptops(apiEndpoint)
    .then(data => {
        laptops = data;
        addLaptopsToMenu(laptops);
    })
    .catch(e => console.log(`Failed to fetch laptops: ${e}`));

const addLaptopsToMenu = (laptops) => {
    laptops.forEach(laptop => addLaptopToMenu(laptop));

    featuresElement.innerHTML = "";
    laptops[0].specs.forEach(feature => {
        const featureElement = document.createElement("li");
        featureElement.value = feature;
        featureElement.appendChild(document.createTextNode(feature));

        featuresElement.appendChild(featureElement);
    });

    detailImageElement.setAttribute('src', laptops[0].image);
    detailImageElement.setAttribute('alt', laptops[0].title);
    detailTitleElement.innerText = laptops[0].title;
    detailDescriptionElement.innerText = laptops[0].description;
    detailPriceElement.innerText = new Intl.NumberFormat('nl-NL', { style: 'currency', currency: 'EUR' }).format(laptops[0].price);
}

const addLaptopToMenu = (laptop) => {
    const laptopElement = document.createElement("option");
    laptopElement.value = laptop.id;
    laptopElement.appendChild(document.createTextNode(laptop.title));

    laptopsElement.appendChild(laptopElement);
}

const handleLaptopMenuChange = e => {
    const selectedLaptop = laptops[e.target.selectedIndex]

    featuresElement.innerHTML = "";
    selectedLaptop.specs.forEach(feature => {
        const featureElement = document.createElement("li");
        featureElement.value = feature;
        featureElement.appendChild(document.createTextNode(feature));

        featuresElement.appendChild(featureElement);
    });

    detailImageElement.setAttribute('src', selectedLaptop.image);
    detailImageElement.setAttribute('alt', selectedLaptop.title);
    detailTitleElement.innerText = selectedLaptop.title;
    detailDescriptionElement.innerText = selectedLaptop.description;
    detailPriceElement.innerText = new Intl.NumberFormat('nl-NL', { style: 'currency', currency: 'EUR' }).format(selectedLaptop.price);
}

const handleWork = () => {
    payBalance += salary;
    payBalanceElement.innerText = new Intl.NumberFormat('nl-NL', { style: 'currency', currency: 'EUR' }).format(payBalance);
}

const handleBank = () => {
    if(loanBalance > payBalance / 10) {
        const loanPayment = payBalance / 10;
        loanBalance -= loanPayment;
        payBalance -= loanPayment;
    } else if(loanBalance > 0) {
        payBalance -= loanBalance;
        loanBalance = 0;
        showLoanElements(false);
    }

    bankBalance += payBalance;
    payBalance = 0;
    updateBalances(); 
}

const handleGetLoan = () => {
    if(loanBalance > 0) {
        alert("You already have a loan, please pay it back first.");
        return;
    }
    const amount = parseInt(window.prompt("How much do you need?", ""));

    if(amount === 0 || isNaN(amount)) {
        alert("You didn't receive a loan. If you still want to receive a loan, please try again.")
        return;
    }

    if(amount > bankBalance * 2) {
        alert(`Your loan limit is ${2 * bankBalance}.`)
        return;
    }

    loanBalance = amount;
    bankBalance += amount;
    updateBalances();
    showLoanElements(true);
}

const showLoanElements = (show:Boolean) => {
    payLoanButtonElement.style.visibility = show ? "visible" : "hidden";
    loanLabelElement.style.visibility = show ? "visible" : "hidden";
    loanBalanceElement.style.visibility = show ? "visible" : "hidden";
    getLoanButtonElement.disabled = show ? true : false;
}

// Make more modulair by supplying the values
const updateBalances = () => {
    payBalanceElement.innerText = new Intl.NumberFormat('nl-NL', { style: 'currency', currency: 'EUR' }).format(payBalance);
    bankBalanceElement.innerText = new Intl.NumberFormat('nl-NL', { style: 'currency', currency: 'EUR' }).format(bankBalance);
    loanBalanceElement.innerText = new Intl.NumberFormat('nl-NL', { style: 'currency', currency: 'EUR' }).format(loanBalance);
}

const handlePayLoan = () => {
    if(loanBalance === 0 || payBalance === 0) return;

    if(loanBalance < payBalance) {
        payBalance -= loanBalance;
        loanBalance = 0;
        showLoanElements(false);
    } else {
        loanBalance -= payBalance;
        payBalance = 0;
    }

    updateBalances();
}

const handleBuy = () => {
    const selectedLaptop = laptops[laptopsElement.selectedIndex];
    const laptopPrice = selectedLaptop.price
    
    if(bankBalance < laptopPrice) {
        alert("Balance too low... Go to work!")
        return;
    }

    bankBalance -= laptopPrice;
    updateBalances();
    alert("Nice you bought a laptop")
}


// All event listeners
laptopsElement.addEventListener("change", handleLaptopMenuChange);
workButtonElement.addEventListener("click", handleWork);
bankButtonElement.addEventListener("click", handleBank);
getLoanButtonElement.addEventListener("click", handleGetLoan);
payLoanButtonElement.addEventListener("click", handlePayLoan);
detailBuyElement.addEventListener("click", handleBuy);
